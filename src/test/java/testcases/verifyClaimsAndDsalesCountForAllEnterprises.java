package testcases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import coreUtilities.testutils.ApiHelper;
import coreUtilities.utils.FileOperations;
import pages.DBQueryExecution;
import pages.StartupPage;
import testBase.AppTestBase;

public class verifyClaimsAndDsalesCountForAllEnterprises extends AppTestBase {
	private String browser;
    private String environment;
    private Boolean isDBTest;
    private String schema;
	String expectedDate="2023-08-19";
	Map<String, String> configData;
	Map<String, String> loginCredentials;
	Connection connection;
	FileOperations file_opr=new FileOperations();
	String sqlQuery = "";
	StartupPage startupPage;
	@BeforeClass(alwaysRun = true)
    public void initParametersFromJenkinsOrTestNG() throws Exception {
		            // Use System.getProperty to retrieve parameters passed from Jenkins
		this.browser = System.getProperty("browser", "chrome");
	    this.environment = System.getProperty("environment", "QA");
	    this.isDBTest = Boolean.parseBoolean(System.getProperty("isDBTestOrOAC", "true"));
	    this.schema = System.getProperty("schema", "iqbiz");
		System.out.println("browser name in jenkins is:::"+this.browser);
		System.out.println("schema namr in jenkins is:::"+this.schema);
			
		logger.info("Inside initParametersFromJenkinsOrTestNG method of BeforeClass");
		if(this.isDBTest!=true)
		{
		configData = new FileOperations().readJson(config_filePath, environment);
		configData.put("url", configData.get("url").replaceAll("[\\\\]", ""));
		configData.put("browser", browser);
		boolean isValidUrl = new ApiHelper().isValidUrl(configData.get("url"));
		Assert.assertTrue(isValidUrl, configData.get("url")+" might be down at this moment. Please try after sometime.");
		initialize(configData);
		startupPage = new StartupPage(driver);
		}
		else
		{	
			System.out.println("inside else block of beforeclass");
			configData = new FileOperations().readJson(db_filepath,schema);
			try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            connection = DriverManager.getConnection("jdbc:oracle:thin:/@(description= (retry_count=20)(retry_delay=3)(address=(protocol=tcps)(port=1522)(host=adb.us-ashburn-1.oraclecloud.com))(connect_data=(service_name=f1icmgkywmxvdbu_adwdev_medium.adb.oraclecloud.com))(security=(ssl_server_dn_match=yes)))", configData.get("username"),configData.get("password"));
	            if (connection != null) {
	                logger.info("Connected to Oracle Database using Wallet!");
 
	            
			}
			}
			catch (ClassNotFoundException e) {
	            System.err.println("Oracle JDBC driver not found. Make sure you have added it to your classpath.");
	        } catch (SQLException e) {
	            System.err.println("Connection failed: " + e.getMessage());
	        }
		}
	}
	@Test(priority = 1, groups = {"sanity"}, description = "comparing the dsales and claims data count with the MV for enterprise 626")
	public void verifyClaimsAndDsalesFor626() throws Exception {		
		logger.info("Inside verifyClaimsAndDsalesFor626 method");
		logger.info("Verifying the claims record count for the enterprise 626");
		ResultSet actualClaimsResultSet= DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from CLAIMS where Key=\'claims_record_count_626\'").get("QUERY")), connection, "626");
		ResultSet expectedClaimsResultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from CLAIMS where Key=\'claims_record_count_626_MV\'").get("QUERY")), connection, "626");
		DBQueryExecution.printResultSet(actualClaimsResultSet);
		DBQueryExecution.printResultSet(expectedClaimsResultSet);
		//Asserting the claims record count
		DBQueryExecution.assertResultSetsEqual(expectedClaimsResultSet, actualClaimsResultSet);
				
		
		logger.info("Verifying the claims record count for the enterprise 626");
		ResultSet actualDSalesResultSet= DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DSALES where Key=\'dsales_record_count_626\'").get("QUERY")), connection, "626");
		ResultSet expectedDSalesResultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DSALES where Key=\'dsales_record_count_626_MV\'").get("QUERY")), connection, "626");
		DBQueryExecution.printResultSet(actualDSalesResultSet);
		DBQueryExecution.printResultSet(expectedDSalesResultSet);
		//Asserting the DSales record count
		DBQueryExecution.assertResultSetsEqual(expectedDSalesResultSet, actualDSalesResultSet);
	} 
	
	@AfterClass(alwaysRun = true)
	public void tearDown() throws SQLException{
		connection.close();
	}
}