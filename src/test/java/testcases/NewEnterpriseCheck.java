package testcases;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import coreUtilities.testutils.ApiHelper;
import coreUtilities.utils.FileOperations;
import pages.DBQueryExecution;
import pages.StartupPage;
import testBase.AppTestBase;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;

import java.sql.SQLException;





public class NewEnterpriseCheck extends AppTestBase {
	private String browser;
    private String environment;
    private Boolean isDBTest;
    private String schema;
	private String enterprise_id;
	String expectedDate="2023-08-19";
	Map<String, String> configData;
	Map<String, String> loginCredentials;
	Connection connection;
	FileOperations file_opr=new FileOperations();
	String sqlQuery = "";
	
	StartupPage startupPage;
	

	@BeforeClass(alwaysRun = true)
   public void initParametersFromJenkinsOrTestNG() throws Exception {
            // Use System.getProperty to retrieve parameters passed from Jenkins
		this.browser = System.getProperty("browser", "chrome");
	    this.environment = System.getProperty("environment", "QA");
	    this.isDBTest = Boolean.parseBoolean(System.getProperty("isDBTestOrOAC", "true"));
	    this.schema = System.getProperty("schema", "iqbiz");
	    this.enterprise_id = System.getProperty("enterprise_id", "640");
        System.out.println("browser name in jenkins is:::"+this.browser);
		logger.info("Inside initParametersFromJenkins method of BeforeClass");
		if(this.isDBTest!=true)
		{
		configData = new FileOperations().readJson(config_filePath, environment);
		configData.put("url", configData.get("url").replaceAll("[\\\\]", ""));
		configData.put("browser", browser);
		
		boolean isValidUrl = new ApiHelper().isValidUrl(configData.get("url"));
		Assert.assertTrue(isValidUrl, configData.get("url")+" might be down at this moment. Please try after sometime.");
		initialize(configData);
		startupPage = new StartupPage(driver);
		}
		else
		{	
			configData = new FileOperations().readJson(db_filepath,schema);
			try {
	            
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            
	            connection = DriverManager.getConnection("jdbc:oracle:thin:/@(description= (retry_count=20)(retry_delay=3)(address=(protocol=tcps)(port=1522)(host=adb.us-ashburn-1.oraclecloud.com))(connect_data=(service_name=f1icmgkywmxvdbu_adwdev_medium.adb.oraclecloud.com))(security=(ssl_server_dn_match=yes)))", configData.get("username"),configData.get("password"));
	            if (connection != null) {
	                logger.info("Connected to Oracle Database using Wallet!");

	            
			}
			}
			catch (ClassNotFoundException e) {
	            System.err.println("Oracle JDBC driver not found. Make sure you have added it to your classpath.");
	        } catch (SQLException e) {
	            System.err.println("Connection failed: " + e.getMessage());
	        }
		}
		
	}
    
	@Test(priority = 1, groups = {"sanity"})
	public void verifyEnterprsieConfigurationTable() throws Exception {		
		logger.info("Inside verifyEnterprsieConfigurationTable method");
		System.out.println("345353534543534");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("ENTERPRISE_ID", "CONFIG_PARAM_NAME","CONFIG_PARAM_VALUE", "CREATE_DT", "CREATE_BY", "UPDATE_DT", "UPDATE_BY"));
		logger.info("Inside verifyEnterprsieConfigurationTable method1111111");
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'ENTERPRISE_CONFIGURATION_Check\'").get("QUERY")),connection,this.enterprise_id);
 
        Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
      //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);
     


	}
	

	@Test(priority = 2, groups = {"sanity"})
	public void verifyDwhEnterpriseAccountTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("ENTERPRISE_ID", "ENT_TYPE","ENT_NAME","CREATE_DT", "CREATED_BY", "REBATE_ENABLED", "ALERT_ENABLED", "IQ_ADMIN_EMAIL", "PROMO_ENABLED", "ITEM_CAT_LEVELS", "LOCATION_REGION_LEVELS", "REGION_LBL_L1", "REGION_LBL_L2", "REGION_LBL_L3", "REGION_LBL_L4", "REGION_LBL_L5", "CHAIN_FILTER_FLAG", "SYNC_REQUEST_FG", "LOAD_INVOICE_REQUEST_FG", "IQ_DIR_NAME", "ITEM_EXTKEYS_FLG", "ODS_LOAD_REQUEST_FLAG", "UNLINKED_LOAD_FLAG", "CAT_LBL_L1", "CAT_LBL_L2", "CAT_LBL_L3", "CAT_LBL_L4", "CAT_LBL_L5", "REPORTING_STEP_ID", "AUTOLINK_EXTKEYS_FLG", "USAGE_REPORT_ENABLED", "SETTING_BIT_PATTERN", "ENT_SHORT_NAME", "PRODUCT_FILTERING_FLAG", "ARAMARK_SPECIFIC_FLAG", "PASSTHROUGH_ENABLED_FLAG", "USE_HTML_REPORTS", "UPDATE_DATE", "UPDATE_BY", "ROLE_ID", "IS_OPTRADE", "DEFAULTAUTH0ROLEID"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_ENTERPRISE_ACCOUNT_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_ENTERPRISE_ACCOUNT_Check\'").get("QUERY")));
        Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 3, groups = {"sanity"})
	public void text_verifyDwhEntCategoryable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("CAT_ID","ENTERPRISE_ID","CAT_DESC", "CAT_CODE", "PARENT_ID", "CREATE_DT", "CREATED_BY", "UPDATE_DT", "PRICE_LIST_CODE", "PURCHASE_GL_CATEGORY_CODE", "UPDATED_BY", "ATTRIBUTE1_VALUE_ID", "ATTRIBUTE2_VALUE_ID", "ATTRIBUTE3_VALUE_ID", "LEVEL_NUM", "LOAD_CODE", "LOGICAL_DELETE_FLAG"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_ENT_CATEGORY_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_ENT_CATEGORY_Check\'").get("QUERY")));
        Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 4, groups = {"sanity"})
	public void text_verifyDwhMasteLocTagsConfigTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("MASTERLOCTAGCONFIGID","ENTERPRISE_ID","ATTRIBUTE_ID","UPDATE_DATE","UPDATE_BY","ALLOWNEW_FLAG","ALLOW_EDIT","GLOBAL_TAG","MERGE_ON_MERGE","ALLOW_MULTIPLE"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_MasterLocTagsConfig_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_MasterLocTagsConfig_Check\'").get("QUERY")));
        Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 5, groups = {"sanity"})
	public void text_verifyDwhDistributorMasterTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("DISTRIBUTOR_ID", "ENTERPRISE_ID", "DIST_CODE", "DIST_NAME", "DIST_ADDRESS", "DIST_CITY", "DIST_STATE", "DIST_ZIP", "DIST_PHONE", "E_MAIL", "E_MAIL_2", "REBATE_ENABLED", "CONTACT_NAME", "ACTIVE_FLAG", "ALERT_ENABLED", "ITEM_CAT_LEVELS", "LOCATION_REGION_LEVELS", "REGION_LBL_L1", "REGION_LBL_L2", "REGION_LBL_L3", "REGION_LBL_L4", "REGION_LBL_L5", "CREATE_DT", "CREATED_BY", "IQ_DIR_NAME", "DIST_CUST_ID", "DIST_ADDRESS_2", "DIST_PROVINCE", "DIST_COUNTRY_CODE", "DIST_FAX", "REBATE_FLAG", "DIST_BEG_DATE", "DIST_END_DATE", "DIST_CATEGORY_CODE", "DUNS_NUMBER", "GU_DUNS_NUMBER", "ALT_DIST_NAME", "IT_CONTACT_NAME", "DIST_ID_CODE", "UPDATED_DATE", "UPDATED_BY", "STG_HOLD_FLAG", "ODS_HOLD_FLAG", "RETURN_HOLD_FLAG", "ALT_DIST_CODE", "MC_DISTRIBUTOR_ID", "DIST_SHORT_NM", "LOAD_CODE", "LAST_INVOICED_DATE", "EXTRACT_MINIMUM_TOTAL"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_DISTRIBUTOR_MASTER_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_DISTRIBUTOR_MASTER_Check\'").get("QUERY")));
        Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 6, groups = {"sanity"})
	public void text_verifyDwhManufacturerTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("MANUF_ID", "ENTERPRISE_ID", "MANUF_CODE", "MANUF_NAME", "MANUF_ADDRESS_1", "MANUF_ADDRESS_2", "MANUF_CITY", "MANUF_STATE", "MANUF_ZIP", "MANUF_PHONE", "MANUF_FAX", "MANUF_EMAIL", "MANUF_URL", "MANUF_UPC", "CREATE_DT", "CREATED_BY", "UNIPRO_SUPP_FLAG", "UNIPRO_SUPP_ID", "UNIPRO_SUPP_BR", "UNIPRO_DUNS_NBR", "UNIPRO_DUNS_SFX", "UPDATE_DT", "UPDATED_BY", "COUNTRY_CODE", "CONTACT_NAME", "REBATE_BEG_DATE", "REBATE_END_DATE", "PURCHASE_MANAGER_CODE", "PARENT_MANUF_ID", "DUNS_NUMBER", "GU_DUNS_NUMBER", "REBATE_ENABLED", "MC_MANUF_ID", "ADMINUSERID", "CREATION_IQA_STEP", "LOAD_CODE", "LOGICAL_DELETE_FLAG"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_MANUFACTURER_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_MANUFACTURER_Check\'").get("QUERY")));
        Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 7, groups = {"sanity"})
	public void text_verifyodsTnaSecRoleTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("ROLE_ID", "ENTERPRISE_ID", "ROLE_NAME", "CREATED_DATE", "CREATED_BY", "UPDATED_DATE", "UPDATED_BY"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'ods_Tna_Sec_Role_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'ods_Tna_Sec_Role_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 8, groups = {"sanity"})
	public void text_verifyodsTnaSecReleTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("ENTERPRISE_ID", "ROLE_ID", "FUNCTION_ID", "PRIV_ID", "CREATED_DATE", "CREATED_BY", "UPDATED_DATE", "UPDATED_BY"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'ods_Tna_Sec_Rel_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'ods_Tna_Sec_Rel_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 9, groups = {"sanity"})
	public void text_verifyDwhEntRegionTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("REGION_ID", "ENT_CHAIN_ID", "PARENT_ID", "DESCRIPTION", "CREATE_DT", "CREATED_BY", "UPDATE_DT", "UPDATED_BY", "LEVEL_NUM", "LOAD_CODE"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_ENT_REGION_Check\'").get("QUERY")),connection,650, true);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'DWH_ENT_REGION_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);

        
	}
	@Test(priority = 10, groups = {"sanity"})
	public void text_verifyIQpipelineExtendedKeysTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("ENTERPRISE_ID", "AUTOLINK_OR_DISTITEM", "DWH_DIST_ITEM_FIELD", "STG_API_INVOICE_FIELD", "CREATE_DATE", "CREATE_BY", "UPDATE_DATE", "UPDATE_BY", "LINKING_GROUP"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'IQPIPELINE_EXTENDED_KEYS_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'IQPIPELINE_EXTENDED_KEYS_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 11, groups = {"sanity"})
	public void text_verifyFormatRuleTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("RULE_ID", "UNIVERSE_TYPE", "SCOPE_TYPE", "SCOPE_VALUE", "APP_NAME", "APP_CONTEXT_LIST", "ENTERPRISE_ID", "PROPERTY_NAME", "PROPERTY_VALUE", "RULE_RANK", "CREATE_BY", "CREATE_DATE", "UPDATE_BY", "UPDATE_DATE"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'FORMAT_RULE_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'FORMAT_RULE_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 12, groups = {"sanity"})
	public void text_verifIqaConfigurationTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("KEYWORD", "VALUE", "ENTERPRISE_ID"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'IQA_CONFIGURATION_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'IQA_CONFIGURATION_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 13, groups = {"sanity"})
	public void text_verifIqaEnterpriseThreadTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("ENTERPRISE_ID", "THREAD_ID"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'IQA_ENTERPRISE_THREAD_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'IQA_ENTERPRISE_THREAD_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 14, groups = {"sanity"})
	public void text_verifIqaSettingsTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("ENTERPRISE_ID", "LOAD_PROCESS", "IMPLIED_DECIMAL_FLG", "REJ_INVOICE_DATE_FLG", "REJ_INVC_DT_FIND_FORMAT_FLG", "REJ_INVC_DT_YYYYMMDD_FLG", "REJ_INVC_DT_MMDDYYYY_FLG", "REJ_INVC_DT_MM_DD_YYYY_FLG", "REJ_INVC_DT_FUTURE_FLG", "REJ_INVC_DT_11MOOLD_FLG", "REJ_GROSS_WGT_DECIMAL_FLG", "REJ_CUST_DIST_ID_FLG", "REJ_CUST_DISTCTR_ID_FLG", "REJ_CUST_COUNTRY_FLG", "REJ_CUST_NUM_FLG", "REJ_CUST_NAME_FLG", "REJ_INVOICE_NUM_FLG", "REJ_ITEM_NUM_FLG", "REJ_ITEM_UOM_FLG", "REJ_INV_QUANTITY_FLG", "REJ_INV_TOTAL_AMOUNT_FLG", "REJ_UNIT_PRICE_FLG", "REJ_UNIT_PRICE_2DEC_FLG", "REJ_UNIT_PRICE_3DEC_FLG", "REJ_UNIT_PRICE_4DEC_FLG", "REJ_ITEM_DESC_FLG", "REJ_DIST_MFG_ID_FLG", "REJ_PACK_UNIT_FLG", "REJ_STGTOODS_RECORD_FLG", "RESET_PRODUCT_INDICATOR_FLG", "RESET_ITEM_SPLIT_FLG", "RESET_RET_REASON_CODE_FLG", "RESET_ITEM_PACK_SIZE_FLG", "RESET_PACK_ITEM_QTY_FLG", "RESET_CUST_COUNTRY_FLG", "RESET_IQ_DISTCTR_ID_FLG", "RESET_INV_TOTAL_AMOUNT_FLG", "RESET_PACK_DATA_FLG", "RESET_CREDIT_VALUES_FLG", "RESET_STANDARD_COUNTRY_FLG", "RESET_DIST_COUNTRY_FLG", "IMPLIED_BCDSIGN_FLG", "RESET_INVOICE_TYPE_CR_DR_FLG", "RESET_PROD_CLASSIF_CODE_FLG", "ROWS_ERROR_PERCENTAGE", "RESET_ITEM_UOM_FLG", "RESET_PACK_UNIT_FLG", "RESET_CREDIT_VALYORN_FLG", "RESET_PREP_ITM_PACK_SIZE_FLG", "IGNORE_WARNINGS_FLG", "MAP_CHILD_MFG_TO_MFG", "RESET_PACK_DATA_DEFAULT_0_FLG", "RESET_PACK_ITEM_QTY_IGN_SIGN_F", "RESET_MANUF_NAME_LID_NUM", "RESET_UNIT_PRICE_4_ROUND_3DEC", "REJ_UNIT_PRICE_10_4DEC_FLG", "REJ_NEGATIVE_UNIT_PRICE_FLG", "REJ_ITM_CATCHWGT_FLG_FLG", "RESET_ITM_CATCHWGT_QTY_FLG", "REJ_NEGATIVE_TOTAL_AMOUNT_FLG", "REJ_NEGATIVE_INV_QUANTITY_FLG", "DEFAULT_ITM_SPLIT_METHOD", "DEFAULT_ITM_SPLIT_CONVERT_FLG", "DEFAULT_NULL_REPLACEMENT", "RESET_ITEM_BRAND_FLG", "RESET_MANUF_NAME_FLG", "RESET_PACK_ITEM_SIZE_3DEC"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'IQA_SETTINGS_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'IQA_SETTINGS_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 15, groups = {"sanity"})
	public void text_verifLtwLogTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("LTW_ID", "OPERATION_CODE", "SCOPE_CODE", "STATUS_CODE", "EPOCH_START_DATE", "EPOCH_END_DATE", "ENTERPRISE_ID", "CREATE_DATE", "CREATE_BY", "UPDATE_DATE", "UPDATE_BY", "RELINK_COMPLETE_FLAG"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'LTW_LOG_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'LTW_LOG_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 16, groups = {"sanity"})
	public void text_verifMngdSubattributeTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("SUBATTRIBUTE_ID", "ATTRIBUTE_ID", "ENTERPRISE_ID", "SUBATTRIBUTE_ATTRIBUTE_ID", "ENFORCE_UNIQUE_VALUES_FLAG", "DISPLAY_ORDER", "CREATE_BY", "CREATE_DATE", "UPDATE_BY", "UPDATE_DATE"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'MNGD_SUBATTRIBUTE_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'MNGD_SUBATTRIBUTE_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 17, groups = {"sanity"})
	public void text_verifyMngdAttributeAllowedVBaseTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("ENTERPRISE_ID", "ATTRIBUTE_ID", "VIRTUAL_DISTINCT_VALUE_ID", "DESCRIPTION", "DEFAULT_VALUE_FLAG", "LOGICAL_DELETE_FLAG", "CREATE_BY", "CREATE_DATE", "UPDATE_BY", "UPDATE_DATE"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'MNGD_ATTRIBUTE_ALLOWED_V_BASE_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'MNGD_ATTRIBUTE_ALLOWED_V_BASE_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);


	}
	@Test(priority = 18, groups = {"sanity"})
	public void text_verifyRptkitSecurityRTemplateTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("RTEMPLATE_SECURITY_ID", "RTEMPLATE_ID", "ENTERPRISE_ID", "USER_ID", "CREATE_BY", "CREATE_DATE", "UPDATE_BY", "UPDATE_DATE", "RTEMPLATE_NAME", "RTEMPLATE_DESC"));
		//LocalDate localDate = LocalDate.of(2023, 8, 19);
	   // Date expectedDate = Date.valueOf(localDate);   
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'RPTKIT_SECURITY_R_TEMPLATE_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'RPTKIT_SECURITY_R_TEMPLATE_Check\'").get("QUERY")));
		
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);
        
        //Assert.assertEquals(DBQueryExecution.fetchDate(resultSet).toString(),expectedDate.toString());
       Assert.assertEquals(DBQueryExecution.fetchDataByColumnName(resultSet,"CREATE_DATE"),expectedDate);
        

	}
	@Test(priority = 19, groups = {"sanity"})
	public void verify_RptkitSecurityQTemplateTable() throws Exception {
		
		logger.info("Inside verifyDwhEntCategoryTable method");
		ArrayList<String> expectedCols = new ArrayList<String>(Arrays.asList("QTEMPLATE_SECURITY_ID", "QTEMPLATE_ID", "ENTERPRISE_ID", "CREATE_BY", "CREATE_DATE", "UPDATE_BY", "UPDATE_DATE", "QTEMPLATE_NAME", "QTEMPLATE_DESC", "QCAT_ID", "REPORTGROUPID"));
		
		ResultSet resultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'RPTKIT_SECURITY_Q_TEMPLATE_Check\'").get("QUERY")),connection,this.enterprise_id);
		logger.info((file_opr.readExcel(db_test_dictionary, "select Query from DWH where Key=\'RPTKIT_SECURITY_Q_TEMPLATE_Check\'").get("QUERY")));
      Assert.assertTrue(DBQueryExecution.findNumberOfRows(resultSet)>0, "there is no records present for the enterprise. Please check manually");
        //Assert the column names in DWH_ENTERPRISE_ACCOUNT_Check table
        Assert.assertEquals(DBQueryExecution.listOfColumns(resultSet), expectedCols);
        
        


	}

	
	
	

	
	@AfterClass(alwaysRun = true)
	public void tearDown() throws SQLException{
		connection.close();
		
	}
}
