package testcases;
import org.testng.annotations.Test;
 
import java.util.ArrayList;
import java.util.Arrays;
 
import java.util.Map;
 
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
 
import coreUtilities.testutils.ApiHelper;
import coreUtilities.utils.FileOperations;
import pages.DBQueryExecution;
import pages.StartupPage;
import testBase.AppTestBase;
import java.sql.Connection;
 
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
 
 
 
public class VerifyTabsLevelsAttributesForAllEnterprises extends AppTestBase {
	private String browser;
    private String environment;
    private Boolean isDBTest;
    private String schema;
	private String enterprise_id;
	String expectedDate="2023-08-19";
	Map<String, String> configData;
	Map<String, String> loginCredentials;
	Connection connection;
	FileOperations file_opr=new FileOperations();
	String sqlQuery = "";
	StartupPage startupPage;
	@BeforeClass(alwaysRun = true)
    public void initParametersFromJenkinsOrTestNG() throws Exception {
		            // Use System.getProperty to retrieve parameters passed from Jenkins
		this.browser = System.getProperty("browser", "chrome");
	    this.environment = System.getProperty("environment", "QA");
	    this.isDBTest = Boolean.parseBoolean(System.getProperty("isDBTestOrOAC", "true"));
	    this.schema = System.getProperty("schema", "iqbiz");
	    this.enterprise_id = System.getProperty("enterprise_id", "640");
		System.out.println("browser name in jenkins is:::"+this.browser);
		System.out.println("enterprise_id in jenkins is:::"+this.enterprise_id);
		System.out.println("schema namr in jenkins is:::"+this.schema);
			
		logger.info("Inside initParametersFromJenkinsOrTestNG method of BeforeClass");
		if(this.isDBTest!=true)
		{
		configData = new FileOperations().readJson(config_filePath, environment);
		configData.put("url", configData.get("url").replaceAll("[\\\\]", ""));
		configData.put("browser", browser);
		boolean isValidUrl = new ApiHelper().isValidUrl(configData.get("url"));
		Assert.assertTrue(isValidUrl, configData.get("url")+" might be down at this moment. Please try after sometime.");
		initialize(configData);
		startupPage = new StartupPage(driver);
		}
		else
		{	
			configData = new FileOperations().readJson(db_filepath,schema);
			try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            if(this.environment.equalsIgnoreCase("Dev"))
	            	connection = DriverManager.getConnection("jdbc:oracle:thin:/@(description= (retry_count=20)(retry_delay=3)(address=(protocol=tcps)(port=1522)(host=adb.us-ashburn-1.oraclecloud.com))(connect_data=(service_name=f1icmgkywmxvdbu_adwdev_medium.adb.oraclecloud.com))(security=(ssl_server_dn_match=yes)))", configData.get("username"),configData.get("password"));
	            else
	            	connection = DriverManager.getConnection("jdbc:oracle:thin:/@(description= (retry_count=20)(retry_delay=3)(address=(protocol=tcps)(port=1522)(host=adb.us-ashburn-1.oraclecloud.com))(connect_data=(service_name=f1icmgkywmxvdbu_adwqap_medium.adb.oraclecloud.com))(security=(ssl_server_dn_match=yes)))", configData.get("username"),configData.get("password"));	
	            if (connection != null) {
	                logger.info("Connected to Oracle Database using Wallet!");
 
	            
			}
			}
			catch (ClassNotFoundException e) {
	            System.err.println("Oracle JDBC driver not found. Make sure you have added it to your classpath.");
	        } catch (SQLException e) {
	            System.err.println("Connection failed: " + e.getMessage());
	        }
		}
	}
	@Test(priority = 1, groups = {"sanity"}, description="verifying the tabs for the enterprise")
	public void verifyAllTheTabs() throws Exception {		
		logger.info("Inside verifyAllTheTabs method");
		ResultSet expectedresultSet= DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from EXPLORE where Key=\'EXTAB_1_EX\'").get("QUERY")),connection,this.enterprise_id);
		ResultSet actualresultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from EXPLORE where Key=\'EXTAB_1_AC\'").get("QUERY")),connection,this.enterprise_id);
		DBQueryExecution.printResultSet(actualresultSet);
		DBQueryExecution.printResultSet(expectedresultSet);
		
		DBQueryExecution.assertResultSetsEqual(expectedresultSet, actualresultSet);
	}
	@Test(priority = 2, groups = {"sanity"})
	public void verifyLabelsTabWise() throws Exception {		
		logger.info("Inside verifyLabelsTabWise method");
		ResultSet expectedresultSet= DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from EXPLORE where Key=\'EXLABEL_1_EX\'").get("QUERY")),connection,this.enterprise_id);
		ResultSet actualresultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from EXPLORE where Key=\'EXLABEL_1_AC\'").get("QUERY")),connection,this.enterprise_id);
		DBQueryExecution.printResultSet(actualresultSet);
		DBQueryExecution.printResultSet(expectedresultSet);
		
		DBQueryExecution.assertResultSetsEqual(expectedresultSet, actualresultSet);
	}
	@Test(priority = 3, groups = {"sanity"})
	public void verifyAttributesBasedUponTabsAndLevels() throws Exception {		
		logger.info("Inside verifyAttributesBasedUponTabsAndLevels method");
		ResultSet expectedresultSet= DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from EXPLORE where Key=\'EXATTRIBUTE_1_EX\'").get("QUERY")),connection,this.enterprise_id);
		ResultSet actualresultSet=DBQueryExecution.executeQuery((file_opr.readExcel(db_test_dictionary, "select Query from EXPLORE where Key=\'EXATTRIBUTE_1_AC\'").get("QUERY")),connection,this.enterprise_id);
		DBQueryExecution.printResultSet(actualresultSet);
		DBQueryExecution.printResultSet(expectedresultSet);
		
		DBQueryExecution.assertResultSetsEqual(expectedresultSet, actualresultSet);
	}
	 


 
	
	@AfterClass(alwaysRun = true)
	public void tearDown() throws SQLException{
		connection.close();
	}
}