package pages;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import org.testng.Assert;
 
 
public class DBQueryExecution {
	public static ResultSet executeQuery(String  query, Connection connection,int number, boolean isRequiredToChange) throws SQLException {
 
		try (PreparedStatement preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
		    if (isRequiredToChange) {
		        preparedStatement.setInt(1, number);
		    }

		    try (ResultSet resultSet = preparedStatement.executeQuery()) {
		        return resultSet;
		    }
		} catch (SQLException e) {
		    e.printStackTrace();
		    throw e;
		}
	}
	
public static ResultSet executeQuery(String  query, Connection connection,String parameter) throws SQLException {
 
		System.out.println("inside executeQuery method");
		PreparedStatement preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE , 
		         ResultSet.CONCUR_UPDATABLE);
		preparedStatement.setString(1,parameter);  

 
        ResultSet resultSet = preparedStatement.executeQuery();
		return resultSet;
	}
 
 
public static int findNumberOfRows(ResultSet resultSet) throws SQLException {
	int counter =0;	
	while (resultSet.next()) {
    	counter++;
        System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"); // Move to the next line for the next row
    }
		System.out.println("counter is + "+counter);
		return counter;
	}
 
public static ArrayList<String> listOfColumns(ResultSet resultSet) throws SQLException {
	ArrayList<String> actualCols = new ArrayList<String>();
	ResultSetMetaData metaData = resultSet.getMetaData();
    int columnCount = metaData.getColumnCount();
 
    for (int i = 1; i <= columnCount; i++) {
    	actualCols.add(metaData.getColumnName(i));
    }
    System.out.println("actual list of cols are : "+actualCols);

	return actualCols;
}
public static String fetchDataByColumnName(ResultSet resultSet,String ColumnName) throws SQLException {

    resultSet.first();

    return resultSet.getDate(ColumnName).toString();
}
public static ResultSet listOfTabs(String  query, Connection connection,String enterpriseName) throws SQLException {
 
		
		PreparedStatement preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE , 
		         ResultSet.CONCUR_UPDATABLE);
		preparedStatement.setString(1,enterpriseName);  

 
        ResultSet resultSet = preparedStatement.executeQuery();
		return resultSet;	
	}
 
public static ArrayList<String> listOfData(ResultSet resultSet,String colName) throws SQLException {
	ArrayList<String> actualCols = new ArrayList<String>();

	resultSet.beforeFirst();
	while (resultSet.next()) {
    	actualCols.add(resultSet.getString(colName));
    }
    System.out.println("actual list of cols are : "+actualCols);

	return actualCols;
}
public static ArrayList<Float> getFristRowData(ResultSet resultSet,String colName) throws SQLException {
	ArrayList<Float> actualCols = new ArrayList<Float>();

	resultSet.beforeFirst();
	while (resultSet.next()) {
    	actualCols.add(resultSet.getFloat(colName));
    }
    System.out.println("actual list of cols are : "+actualCols);

	return actualCols;
}
public static ResultSet executeQuery(String  query, Connection connection,int number,int StartDate,int EndDate) throws SQLException {
 
	
	PreparedStatement preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE , 
	         ResultSet.CONCUR_UPDATABLE);
	preparedStatement.setInt(1,number); 
	preparedStatement.setInt(2,StartDate);
	preparedStatement.setInt(3,EndDate);

 
    ResultSet resultSet = preparedStatement.executeQuery();
	return resultSet;
}
 
public static ArrayList<String> listOfLabels(ResultSet resultSet,String colName) throws SQLException {
	ArrayList<String> actualCols = new ArrayList<String>();

	resultSet.beforeFirst();
	while (resultSet.next()) {
    	actualCols.add(resultSet.getString(colName));
    }
    System.out.println("actual list of cols are : "+actualCols);

	return actualCols;
}
 

public static void printResultSet(ResultSet resultSet) {
    try {
        ResultSetMetaData metaData = resultSet.getMetaData();

        // Print column names
        int columnCount = metaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
            System.out.print(metaData.getColumnName(i) + "\t");
        }
        System.out.println();

        // Print data
        while (resultSet.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.print(resultSet.getString(i) + "\t");
            }
            System.out.println();
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
}

public static void assertResultSetsEqual(ResultSet expected, ResultSet actual) throws SQLException {
    ResultSetMetaData expectedMetaData = expected.getMetaData();
    ResultSetMetaData actualMetaData = actual.getMetaData();

    // columns
    Assert.assertEquals(expectedMetaData.getColumnCount(), actualMetaData.getColumnCount(),
            "Number of columns in the result sets do not match");

    //column names,data types
    for (int i = 1; i <= expectedMetaData.getColumnCount(); i++) {
        Assert.assertEquals(expectedMetaData.getColumnName(i), actualMetaData.getColumnName(i),
                "Column names do not match");

        Assert.assertEquals(expectedMetaData.getColumnType(i), actualMetaData.getColumnType(i),
                "Column data types do not match");
    }

    // Data
    while (expected.next() && actual.next()) {
        for (int i = 1; i <= expectedMetaData.getColumnCount(); i++) {
            Object expectedValue = expected.getObject(i);
            Object actualValue = actual.getObject(i);

            // Use your custom logic to compare values, for simplicity, using equals() here
            Assert.assertEquals(expectedValue, actualValue,
                    "Values in the result sets do not match");
        }
    }

    // row count
    Assert.assertFalse(expected.next() || actual.next(),
            "Number of rows in the result sets do not match");
}

}
