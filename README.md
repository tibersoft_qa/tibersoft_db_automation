# Tibersoft_Explore_Automation




## Create Materialized Views

- Materialized View for Explore Tabs

```
CREATE MATERIALIZED VIEW EXPLORE_QA_TABS
BUILD IMMEDIATE
REFRESH ON DEMAND
AS
select
           /*+ LEADING(vl MV fe bcl) USE_HASH(vl MV fe bcl) */
           DISTINCT      mv.enterprise_id,
           mv.caption AS tab
FROM       dwh_format_expansion fe
INNER JOIN be_cfg_view_levels vl
ON         fe.rptkit_column_name = vl.level_column
INNER JOIN be_cfg_mart_views mv
ON         vl.view_id = mv.view_id
AND        fe.enterprise_id = mv.enterprise_id
LEFT JOIN  be_cfg_level_cols bcl
ON         vl.level_column = bcl.level_column
AND        fe.enterprise_id = bcl.enterprise_id
AND        vl.level_column <> bcl.column_name
AND        vl.level_display_column <> bcl.column_name
AND        bcl.column_name NOT LIKE '%||%'
ORDER BY   1 DESC
;
```

- Materialized View for Explore Labels

```
CREATE MATERIALIZED VIEW EXPLORE_QA_LABELS
BUILD IMMEDIATE
REFRESH ON DEMAND
AS
select
           /*+ LEADING(vl MV fe bcl) USE_HASH(vl MV fe bcl) */
           DISTINCT      mv.enterprise_id,
           mv.caption AS tab,
           vl.caption AS Label
FROM       dwh_format_expansion fe
INNER JOIN be_cfg_view_levels vl
ON         fe.rptkit_column_name = vl.level_column
INNER JOIN be_cfg_mart_views mv
ON         vl.view_id = mv.view_id
AND        fe.enterprise_id = mv.enterprise_id
LEFT JOIN  be_cfg_level_cols bcl
ON         vl.level_column = bcl.level_column
AND        fe.enterprise_id = bcl.enterprise_id
AND        vl.level_column <> bcl.column_name
AND        vl.level_display_column <> bcl.column_name
AND        bcl.column_name NOT LIKE '%||%'
--WHERE      fe.enterprise_id = ?
--             AND mv.CAPTION = 'Sales'
           -- AND vl.caption = 'Redistributor'
ORDER BY   1 DESC;
```

- Materialized View for Explore Attributes 

```
CREATE MATERIALIZED VIEW EXPLORE_QA_ATTRIBUTES
BUILD IMMEDIATE
REFRESH ON DEMAND
AS
SELECT /*+ LEADING(vl MV fe bcl) USE_HASH(vl MV fe bcl) */ 
   DISTINCT
           mv.enterprise_id,
           mv.caption                 AS TAB,
           vl.caption                 AS Label,
           fe.LABEL                   AS "ATTRIBUTE1",
           fe.DESCRIPTION as DESCRIPTION1,
           vl.level_column            AS "ATTRIBUTE COLUMN1",
           CASE
               WHEN vl.level_display_column <> vl.level_column 
               THEN
                   (SELECT fe2.LABEL
                      FROM DWH_FORMAT_EXPANSION fe2
                     WHERE     fe2.RPTKIT_COLUMN_NAME = vl.level_display_column
                           AND fe2.ENTERPRISE_ID = mv.enterprise_id
                           )
               ELSE
                   'Not Available'
           END AS "ATTRIBUTE2",
           CASE
               WHEN vl.level_display_column <> vl.level_column 
               THEN
                   (SELECT fe2.DESCRIPTION
                      FROM DWH_FORMAT_EXPANSION fe2
                     WHERE     fe2.RPTKIT_COLUMN_NAME = vl.level_display_column
                           AND fe2.ENTERPRISE_ID = mv.enterprise_id
                           )
               ELSE
                   'Not Available'
           END AS "DESCRIPTION2",           
           CASE
               WHEN vl.level_display_column <> vl.level_column 
               THEN vl.level_display_column
               ELSE 'Not Available'
           END AS "ATTRIBUTE COLUMN2",
           CASE
               WHEN bcl.column_name IS NOT NULL
               THEN
                   (SELECT fe2.LABEL
                      FROM DWH_FORMAT_EXPANSION fe2
                     WHERE     fe2.RPTKIT_COLUMN_NAME = bcl.column_name
                           AND fe2.ENTERPRISE_ID = mv.enterprise_id
                           )
               ELSE
                   'Not Available'
           END AS "ATTRIBUTE3",
           CASE
               WHEN bcl.column_name IS NOT NULL 
               THEN
                   (SELECT fe2.DESCRIPTION
                      FROM DWH_FORMAT_EXPANSION fe2
                     WHERE     fe2.RPTKIT_COLUMN_NAME = bcl.column_name
                           AND fe2.ENTERPRISE_ID = mv.enterprise_id
                           )
               ELSE
                  'Not Available'
           END AS "DESCRIPTION3",
          NVL(bcl.column_name, 'Not Available') AS "ATTRIBUTE COLUMN3"
      FROM DWH_FORMAT_EXPANSION  fe
           INNER JOIN be_cfg_view_levels vl
               ON fe.rptkit_column_name = vl.level_column
           INNER JOIN BE_CFG_MART_VIEWS mv
               ON vl.VIEW_ID = mv.VIEW_ID AND fe.ENTERPRISE_ID = mv.ENTERPRISE_ID
           left JOIN BE_CFG_LEVEL_COLS bcl
               ON vl.level_column = bcl.level_column 
                  AND fe.ENTERPRISE_ID = bcl.ENTERPRISE_ID 
                  AND vl.level_column <> bcl.column_name
                  AND vl.level_display_column <> bcl.column_name
                  and bcl.COLUMN_NAME not like '%||%'
--      WHERE fe.enterprise_id = 693
--        AND mv.CAPTION = 'Claims'
--       AND vl.caption = 'Sub Brand'
ORDER BY 1 DESC;
```



## Materialized View Refresh

- Syntax
```
EXEC DBMS_MVIEW.REFRESH('your_materialized_view', 'C', atomic_refresh => FALSE);
```